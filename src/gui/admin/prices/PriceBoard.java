package gui.admin.prices;

import java.util.Optional;

import gui.admin.template.PriceOperation;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can decide a specific price of a specific board type.
 *
 */
public class PriceBoard extends PriceOperation {
    /**
     * 
     * @param descrizione
     *            is a test of a label in PriceOperation
     * 
     */
    public PriceBoard(final String descrizione) {
        super(descrizione);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendPrice(final Double p, final String name) {
        Optional<StayTypePriceDescriber> opt = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                .filter(type -> type.getDescription().equals(name)).findFirst();
        HotelManager.create().setPriceDescriber(opt.get(), p);
    }
}