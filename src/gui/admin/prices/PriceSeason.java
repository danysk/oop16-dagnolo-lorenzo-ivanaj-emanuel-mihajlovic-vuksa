package gui.admin.prices;

import java.util.Optional;

import gui.admin.template.PriceOperation;
import hotelmaster.pricing.SeasonPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can modify the price of a specific season.
 *
 */
public class PriceSeason extends PriceOperation {
    /**
     * 
     * @param descrizione
     *            is the text of a label that will be in PriceOperation
     */
    public PriceSeason(final String descrizione) {
        super(descrizione);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(SeasonPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendPrice(final Double price, final String name) throws IllegalArgumentException {
        if (price <= 0) {
            throw new IllegalArgumentException();
        }
        Optional<SeasonPriceDescriber> opt = Hotel.instance().getPriceView(SeasonPriceDescriber.class).stream()
                .filter(type -> type.getDescription().equals(name)).findFirst();
        HotelManager.create().setPriceDescriber(opt.get(), price);
    }
}
