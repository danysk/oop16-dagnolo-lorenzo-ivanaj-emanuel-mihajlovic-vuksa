package gui.admin.mainview;

import java.awt.event.ActionListener;

import gui.admin.account.NewAccount;
import gui.admin.boardtype.CreaPensione;
import gui.admin.floor.CreaPiano;
import gui.admin.person.CreazionePersone;
import gui.admin.roomextra.CreaSupplementoCamera;
import gui.admin.rooms.CreaCamera;
import gui.admin.roomtype.CreazioneTipoCamera;
import gui.admin.season.CreaStagione;
import gui.admin.stayextra.CreaSupplementoSoggiorno;
import gui.admin.template.Operations;

/**
 * 
 * this is the frame where the admin can create something.
 *
 */
public class Crea extends Operations {
    @Override
    public ActionListener camera() {
        return (l) -> {
            new CreaCamera();
            this.shut();
        };
    }

    @Override
    public ActionListener piano() {
        return (l) -> {
            new CreaPiano();
            this.shut();
        };
    }

    @Override
    public ActionListener account() {
        return (l) -> {
            new NewAccount();
            this.shut();
        };
    }

    @Override
    public ActionListener supplementoSoggiorno() {
        return (l) -> {
            new CreaSupplementoSoggiorno();
            this.shut();
        };
    }

    @Override
    public ActionListener supplementoCamera() {
        return (l) -> {
            new CreaSupplementoCamera();
            this.shut();
        };
    }

    @Override
    public ActionListener tipoCamera() {
        return (l) -> {
            new CreazioneTipoCamera();
            this.shut();
        };
    }

    @Override
    public ActionListener tipoPensione() {
        return (l) -> {
            new CreaPensione();
            this.shut();
        };
    }

    @Override
    public ActionListener persone() {
        return (l) -> {
            new CreazionePersone();
            this.shut();
        };
    }

    @Override
    public ActionListener stagione() {
        return (l) -> {
            new CreaStagione();
            this.shut();
        };
    }

    @Override
    public ActionListener esci() {
        return (l) -> {
            new Scelte();
            this.shut();
        };
    }

}
