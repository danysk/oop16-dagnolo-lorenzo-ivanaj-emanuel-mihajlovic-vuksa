package gui.admin.season;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.SeasonPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can delete a specific season.
 *
 */
public class CancellaStagione extends DeleteOperation {
    /**
     * 
     * @param testo
     *            is the text of a specific label
     * @param titolo
     *            is the text of the frame
     */
    public CancellaStagione(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(SeasonPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<SeasonPriceDescriber> opt = Hotel.instance().getPriceView(SeasonPriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
        HotelManager.create().removePriceDescriber(opt.get());
    }
}