package gui.secretary.confirmreservation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.reservations.InactiveStayManager;
import hotelmaster.reservations.Occupation;
import hotelmaster.reservations.Stay;

/**
 * 
 * emanu.
 *
 */
public class ViewReservation {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Dimension screenSize;
    private Image ok;
    private Image exitIcon;
    private JButton conferma;
    private JButton esci;
    private String doc;
    private Font font;

    // CHECKSTYLE:OFF: MagicNumber
    /**
     * Show all rooms that a person has reserved.
     * 
     * @param documento
     *            number of a document of a client
     */
    public ViewReservation(final Stay stay) {
        this.frame = new JFrame();
        this.frame.setSize(new Dimension(600, 500));
        this.panel = new JPanel();
        this.panel.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.cyan);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        for (Occupation occupazione : stay.getOccupationsView()) {
            JLabel label = new JLabel(occupazione.getRoom().getID().getFullID());
            label.setFont(this.font);
            this.panel.add(label);
        }
        this.font = new Font("Baskerville", Font.HANGING_BASELINE, 30);
        this.ok = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.ok));
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.conferma.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi confermare la prenotazione?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                try {
                    ((InactiveStayManager) stay.getStayManager()).checkin();
                } catch (IllegalStateException e1) {
                    JOptionPane.showMessageDialog(this.frame,
                            "Impossibile confermare la prenotazione: non inizia oggi", "Errore",
                            JOptionPane.ERROR_MESSAGE);
                }
                this.frame.setVisible(false);
                this.frame.dispose();
                new SceltaOpzione();
            }
        });
        this.esci.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new SceltaOpzione();
        });
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);

    }
}
