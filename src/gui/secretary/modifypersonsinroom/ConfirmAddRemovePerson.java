package gui.secretary.modifypersonsinroom;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.exceptions.OccupiedRoomException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.reservations.InactiveStayManager;
import hotelmaster.reservations.Stay;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.Room;

/**
 * 
 * emanu.
 *
 */
public class ConfirmAddRemovePerson {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Dimension screenSize;
    private JButton conferma;
    private JButton esci;
    private Image exitIcon;
    private Image ok;
    private Map<JTextField, PersonPriceDescriber> personeselezionate;
    private JComboBox<String> combo;
    private Map<String, Room> stanze;

    // CHECKSTYLE:OFF: MagicNumber.
    /**
     * 
     * @param document
     *            is the number of document.
     */
    public ConfirmAddRemovePerson(final Stay stay) {
        this.personeselezionate = new HashMap<>();
        this.frame = new JFrame();
        this.frame.setSize(new Dimension(500, 500));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.stanze = new HashMap<>();
        this.panel = new JPanel();
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        this.ok = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.ok));
        this.esci = new JButton("");

        for (Room stanza : stay.getOccupationsView().stream().map(occupazione -> occupazione.getRoom())
                .collect(Collectors.toList())) {
            this.stanze.put(stanza.getID().getFullID(), stanza);
        }
        this.combo = new JComboBox<>(this.stanze.keySet().toArray(new String[0]));
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        for (PersonPriceDescriber persona : Hotel.instance().getPriceView(PersonPriceDescriber.class)) {
            JLabel tipopersone = new JLabel(persona.getDescription());
            this.panel.add(tipopersone);
            JTextField numpersone = new JTextField(5);
            numpersone.setText("0");
            this.panel.add(numpersone);
            this.personeselezionate.put(numpersone, persona);
        }
        this.conferma.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Vuoi confermare la prenotazione?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                final Map<PersonPriceDescriber, Integer> personeinstanza = new HashMap<>();
                try {
                    for (final JTextField num : personeselezionate.keySet()) {
                        final int numero = Integer.parseInt(num.getText().toString());
                        if (numero > 0) {
                            personeinstanza.put(personeselezionate.get(num), numero);
                        }
                    }
                    System.out.println(personeinstanza);
                    System.out.println(((InactiveStayManager) stay.getStayManager())
                            .addRoom(stanze.get(combo.getSelectedItem().toString()), personeinstanza));
                } catch (IllegalArgumentException e1) {
                    JOptionPane.showMessageDialog(this.frame, "Si possono inserire solo numeri positivi");
                } catch (MissingEntityException e1) {
                    JOptionPane.showMessageDialog(this.frame, "Camera non trovata");
                } catch (OccupiedRoomException e1) {
                    e1.printStackTrace();
                } catch (IllegalStateException e1) {
                    e1.printStackTrace();
                }
            }
        });
        this.esci.addActionListener(e ->

        {
            this.frame.setVisible(false);
            this.frame.dispose();
            new SceltaOpzione();
        });
        this.panel.add(combo);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

}
