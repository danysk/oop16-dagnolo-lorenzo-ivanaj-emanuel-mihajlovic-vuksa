package gui.secretary.entryday;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * emanu.
 *
 */
public class ModificaDataIngresso extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the label text
     */
    public ModificaDataIngresso(final String testo) {
        super(testo, StayState.INACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new DataIngresso(stay);

    }
}
