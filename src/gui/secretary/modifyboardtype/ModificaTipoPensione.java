package gui.secretary.modifyboardtype;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * Modify board type.
 *
 */
public class ModificaTipoPensione extends DocumentTemplate {
    /**
     * 
     * @param testo
     *            is the text of label
     */
    public ModificaTipoPensione(final String testo) {
        super(testo, StayState.INACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new ModifyBoardType(stay);

    }

}
