package gui.secretary.modifyboardtype;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.documentamplate.DocumentTemplate;
import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.reservations.InactiveStayManager;
import hotelmaster.reservations.Stay;
import hotelmaster.structure.Hotel;

//: MagicNumber
/**
 * 
 * here the secretary, can modify a board type.
 *
 */
public class ModifyBoardType {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JLabel label;
    private JButton conferma;
    private JButton annulla;
    private Dimension screenSize;
    private Image ok;
    private Image backIcon;
    private GridBagConstraints grid;
    private JComboBox<String> combo;
    private String[] str;
    private JLabel labelType;
    private String scelta;

    /**
     * 
     * @param doc
     *            is the number of a specific document number.
     */
    public ModifyBoardType(final Stay stay) {
        this.frame = new JFrame();
        this.frame.setSize(new Dimension(500, 500));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel(new GridBagLayout());
        this.grid = new GridBagConstraints();
        this.southPanel = new JPanel();
        this.str = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).collect(Collectors.toList()).toArray(new String[0]);
        this.labelType = new JLabel("Pensione attuale   " + stay.getType().getDescription());
        this.grid.gridx = 0;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.labelType, this.grid);
        this.label = new JLabel("Scegli il nuovo tipo di pensione");
        this.grid.gridx = 0;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.label, this.grid);
        this.combo = new JComboBox<>(this.str);
        this.combo.setBackground(Color.YELLOW);
        this.grid.gridx = 1;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.panel.add(this.combo, this.grid);
        this.ok = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.ok));
        this.annulla = new JButton("");
        this.annulla.setIcon(new ImageIcon(this.backIcon));
        this.conferma.addActionListener(e -> {
            this.scelta = this.combo.getSelectedItem().toString();
            try {
                StayTypePriceDescriber nuovotipo = Hotel.instance().getPriceView(StayTypePriceDescriber.class).stream()
                        .filter(tipo -> tipo.getDescription().equals(this.scelta)).findFirst().get();
                ((InactiveStayManager) stay.getStayManager()).setType(nuovotipo);
                JOptionPane.showMessageDialog(this.frame, "Tipo di pensione modificato");
            } catch (MissingEntityException e1) {
                JOptionPane.showMessageDialog(this.frame, "Pensione non trovata");
            } catch (IllegalStateException e1) {
                JOptionPane.showMessageDialog(this.frame, "Pensione non piu valida");
            }
            new SceltaOpzione();
        });
        this.southPanel.add(this.annulla);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }
}