package gui.secretary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.google.common.base.Optional;

import gui.admin.mainview.Scelte;
import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.database.login.LoginManager;
import hotelmaster.database.login.LoginManagerImpl;


//
/**
 * Gui login, here, the secretary and admin can validate their account.
 *
 */
public class LogIn {
    private JFrame frame;
    private JPanel northPanel;
    private JPanel pannelloDati;
    private JPanel southPanel;
    private JButton log;
    private JTextField text;
    private JPasswordField pass;
    private JLabel labelUtente;
    private JLabel labelPw;
    private JLabel titolo;
    private JLabel icona;
    private String rispMail;
    private String password;
    private Dimension screenSize;
    private Image logg;
    private LoginManager loginmanager;
    private Optional<hotelmaster.database.login.AccountLevel> account;
    private GridBagConstraints grid;
    private Font font;

    /**
     * 
     */
    public LogIn() {
        this.frame = new JFrame("Login");
        this.frame.setSize(700, 500);
        this.frame.setMinimumSize(new Dimension(200, 400));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setLocationRelativeTo(null);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.loginmanager = new LoginManagerImpl();
        this.northPanel = new JPanel(new GridBagLayout());
        this.grid = new GridBagConstraints();
        this.northPanel.setBackground(Color.cyan);
        this.pannelloDati = new JPanel(new GridBagLayout());
        this.pannelloDati.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.cyan);
        this.logg = new ImageIcon(this.getClass().getResource("/icons/log.png")).getImage();
        this.log = new JButton("Login");
        this.titolo = new JLabel("Hotel Master");
        this.icona = new JLabel("");
        this.icona.setIcon(new ImageIcon(this.logg));
        this.southPanel.add(this.icona);
        this.southPanel.add(this.log);
        this.font = new Font("Baskerville", Font.HANGING_BASELINE, 60);
        this.titolo.setFont(font);
        this.grid.gridx = 0;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.northPanel.add(this.titolo, this.grid);
        this.labelUtente = new JLabel("Inserisci nome utente");
        this.grid.gridx = 0;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.pannelloDati.add(this.labelUtente, this.grid);
        this.text = new JTextField(20);
        this.text.setBackground(Color.yellow);
        this.text.setPreferredSize(new Dimension(50, 30));
        this.grid.gridx = 1;
        this.grid.gridy = 0;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.pannelloDati.add(this.text, this.grid);
        this.labelPw = new JLabel("Inserisci password");
        this.grid.gridx = 0;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.pannelloDati.add(this.labelPw, this.grid);
        this.pass = new JPasswordField(20);
        this.pass.setPreferredSize(new Dimension(50, 30));
        this.pass.setBackground(Color.yellow);
        this.grid.gridx = 1;
        this.grid.gridy = 1;
        this.grid.insets = new Insets(10, 10, 10, 10);
        this.pannelloDati.add(this.pass, this.grid);
        this.log.addActionListener(e -> {
            this.password = new String(this.pass.getPassword());
            this.text.selectAll();
            this.rispMail = this.text.getText();
            this.account = this.loginmanager.logIn(this.rispMail, this.password);
            if (account.isPresent()) {
                switch (account.get()) {
                case ADMIN:
                    this.frame.setVisible(false);
                    this.frame.dispose();
                    new Scelte();
                    break;
                case SECRETARY:
                    new SceltaOpzione();
                    this.frame.setVisible(false);
                    this.frame.dispose();
                default:
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Opsss, qualcosa e andato storto, riprova per favore",
                        "Errore login", JOptionPane.ERROR_MESSAGE);
            }
        });
        this.frame.getContentPane().add(this.northPanel, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.pannelloDati);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }
}