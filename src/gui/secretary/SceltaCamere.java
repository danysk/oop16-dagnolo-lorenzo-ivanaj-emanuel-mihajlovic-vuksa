package gui.secretary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.secretary.mainview.SceltaOpzione;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.exceptions.OccupiedRoomException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.reservations.StayBuilderSecondStep;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.Room;

/**
 * 
 * here the secretary will choose the room/rooms that will be occupied.
 *
 */
public class SceltaCamere {
    private JFrame frame;
    private List<JButton> button;
    private JButton indietro;
    private JButton esci;
    private JButton conferma;
    private JCheckBox filtri;
    private JPanel pannelloFiltri;
    private JPanel pannelloBottoni;
    private JPanel southPanel;
    private Optional<String> tipo;
    private List<String> suppl;
    private Map<Room, Map<PersonPriceDescriber, Integer>> stanzeselezionate;
    private StayBuilderSecondStep staybuilder;

    // CHECKSTYLE:OFF: MagicNumber
    /**
     * 
     * */
    public SceltaCamere(final StayBuilderSecondStep staybuilder) {
        this.staybuilder = staybuilder;
        this.frame = new JFrame("Hotel Master");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pannelloFiltri = new JPanel(new GridLayout(1, 1));
        this.pannelloFiltri.setPreferredSize(new Dimension(0, 40));
        this.pannelloFiltri.setBackground(Color.CYAN);
        this.pannelloBottoni = new JPanel(new GridLayout(15, 10));
        this.southPanel = new JPanel();
        this.suppl = new ArrayList<>();
        this.tipo = Optional.empty();
        this.stanzeselezionate = new HashMap<>();
        this.button = new ArrayList<>();
        this.filtri = new JCheckBox("Utilizza filtri");
        this.filtri.addActionListener(e -> {
            new Filtri(this);
        });
        this.esci = new JButton("Esci");
        this.indietro = new JButton("Indietro");
        this.conferma = new JButton("Conferma");
        this.esci.addActionListener(b -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere uscire?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                new LogIn();
            }
        });
        this.indietro.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere tornare indietro?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new FaseRegistrazione();
            }
        });
        this.conferma.addActionListener(e -> {
            int risp = JOptionPane.showConfirmDialog(this.frame,
                    "Sei sicuro di voler confermare la prenotazione di questa/e camera/e?", "Conferma",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {

                for (Room r : stanzeselezionate.keySet()) {
                    try {
                        staybuilder.addRoom(r, stanzeselezionate.get(r));
                    } catch (MissingEntityException e1) {
                        e1.printStackTrace();
                    } catch (IllegalArgumentException e1) {
                        e1.printStackTrace();
                    } catch (IllegalStateException e1) {
                        e1.printStackTrace();
                    } catch (OccupiedRoomException e1) {
                        e1.printStackTrace();
                    }
                }
                staybuilder.complete();
                Hotel.instance().getStayView().stream().forEach(c -> {
                    System.out.println(c.getClient());
                    c.getExtrasView().stream().forEach(b -> {
                        System.out.println(b.getDescription());
                    });
                });

                this.frame.setVisible(false);
                this.frame.dispose();
                new SceltaOpzione();
            }
        });
        setFilters(tipo, suppl, new HashMap<>());
        this.southPanel.add(this.indietro);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.pannelloFiltri.add(this.filtri);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.pannelloFiltri, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.pannelloBottoni);
        this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.frame.setVisible(true);
    }

    /**
     * 
     * @param tipo
     *            is type of room
     * @param suppl
     *            is the supplement
     * @param persone
     *            is the number of persons
     */

    public void setFilters(final Optional<String> tipo, final List<String> suppl,
            final Map<PersonPriceDescriber, Integer> persone) {
        this.pannelloBottoni.removeAll();
        this.pannelloBottoni.revalidate();
        this.pannelloBottoni.repaint();
        button.clear();
        List<RoomExtraPriceDescriber> supplementiStanza = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class)
                .stream().filter(sup -> suppl.contains(sup.getDescription())).collect(Collectors.toList());
        List<Room> stanze = Hotel.instance().getRoomView().stream().filter(stanza -> {

            if (tipo.isPresent() && !stanza.getType().getDescription().equals(tipo.get())) {
                System.out.println(stanza.toString() + " eliminata per tipo");
                return false;
            }
            if (stanza.isOccupiedOn(staybuilder.getDates().get())) {
                return false;
            }
            if (!stanza.getExtrasView().containsAll(supplementiStanza)) {
                return false;
            }
            if (persone.values().stream().mapToInt(Integer::intValue).sum() > stanza.getType().getMaxPeople()) {
                return false;
            }
            return true;
        }).sorted().collect(Collectors.toList());
        Collections.sort(stanze);
        for (Room stanza : stanze) {

            JButton b = new JButton(stanza.getID().getFullID() + " " + stanza.getType());
            b.setBackground(Color.GREEN);
            if (this.stanzeselezionate.containsKey(stanza)) {
                b.setBackground(Color.red);
                b.setEnabled(false);
            }
            b.addActionListener(a -> {
                if (persone.entrySet().stream().mapToInt(p -> p.getValue()).sum() == 0) {
                    JOptionPane.showMessageDialog(this.frame,
                            "Seleziona il numero  delle delle persone nella camera tramite i filtri");
                } else {
                    int risp = JOptionPane.showConfirmDialog(this.frame,
                            "Sei sicuro di voler aggiungere la camera " + b.getText() + "? ", "Conferma",
                            JOptionPane.YES_OPTION);
                    if (risp == JOptionPane.YES_OPTION) {
                        b.setBackground(Color.RED);
                        b.setEnabled(false);
                        this.stanzeselezionate.put(stanza, persone);
                    }
                }
            });
            this.pannelloBottoni.add(b);
        }
    }
}