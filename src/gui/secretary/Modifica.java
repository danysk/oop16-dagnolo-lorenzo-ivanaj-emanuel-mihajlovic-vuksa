package gui.secretary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import gui.secretary.closereservation.ConfirmCloseReservation;
import gui.secretary.confirmreservation.ConfermaPrenotazione;
import gui.secretary.entryday.ModificaDataIngresso;
import gui.secretary.exitdate.NewData;
import gui.secretary.mainview.SceltaOpzione;
import gui.secretary.modifyboardtype.ModificaTipoPensione;
import gui.secretary.modifypassword.ModificaPassword;
import gui.secretary.modifypersonsinroom.AddRemovePerson;
import gui.secretary.staysupplement.AddStaySupplement;

//
/**
 * 
 * Here, the secretary can modify something using the checkbox.
 *
 */
public class Modifica {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JCheckBox checkModificaSogg;
    private JCheckBox checkConfermaPrenotazione;
    private JCheckBox checkModificaIng;
    private JCheckBox checkBoardType;
    private JCheckBox checkAddInRoom;
    private JCheckBox checkChiudiPrenotazione;
    private JCheckBox checkAddSoggiorno;
    private JCheckBox checkPassword;
    private JButton indietro;
    private JButton ok;
    private Image backIcon;
    private Image okIcon;
    private Dimension screenSize;

    /**
     * 
     */
    public Modifica() {
        this.frame = new JFrame("Modifica");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(new Dimension(500, 200));
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((this.screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.panel = new JPanel(new GridLayout(5, 1));
        this.panel.setBackground(Color.CYAN);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);

        this.checkModificaIng = new JCheckBox("Modifica data ingresso");
        this.checkModificaIng.setBackground(Color.cyan);
        this.checkModificaIng.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ModificaDataIngresso("Modifica data ingresso");
        });
        this.checkModificaSogg = new JCheckBox("Modifica data uscita");
        this.checkModificaSogg.setBackground(Color.cyan);
        this.checkModificaSogg.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new NewData("Modifica data uscita");
        });
        this.checkConfermaPrenotazione = new JCheckBox("Conferma Prenotazione");
        this.checkConfermaPrenotazione.setBackground(Color.cyan);
        this.checkConfermaPrenotazione.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ConfermaPrenotazione("Conferma prenotazione");
        });
        this.checkChiudiPrenotazione = new JCheckBox("Annulla Prenotazione");
        this.checkChiudiPrenotazione.setBackground(Color.CYAN);
        this.checkChiudiPrenotazione.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ConfirmCloseReservation("Annulla prenotazione");
        });
        this.checkBoardType = new JCheckBox("Modifica Tipo Pensione");
        this.checkBoardType.setBackground(Color.CYAN);
        this.checkBoardType.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ModificaTipoPensione("Modifica tipo pensione");
        });
        this.checkAddSoggiorno = new JCheckBox("Modifica supplementi");
        this.checkAddSoggiorno.setBackground(Color.CYAN);
        this.checkAddSoggiorno.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new AddStaySupplement("Modifica supplementi soggiorno");
        });
        this.checkAddInRoom = new JCheckBox("Modifica persone in stanza");
        this.checkAddInRoom.setBackground(Color.cyan);
        this.checkAddInRoom.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new AddRemovePerson("Modifica persone in stanza");
        });
        this.checkPassword = new JCheckBox("Modifica Password");
        this.checkPassword.setBackground(Color.cyan);
        this.checkPassword.addActionListener(e -> {
            this.frame.setVisible(false);
            this.frame.dispose();
            new ModificaPassword();
        });

        this.panel.add(checkModificaIng);
        this.panel.add(checkModificaSogg);
        this.panel.add(checkConfermaPrenotazione);
        this.panel.add(checkChiudiPrenotazione);
        this.panel.add(checkBoardType);
        this.panel.add(checkAddSoggiorno);
        this.panel.add(checkAddInRoom);
        this.panel.add(checkPassword);

        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.indietro = new JButton("");
        this.indietro.setIcon(new ImageIcon(this.backIcon));
        this.ok = new JButton("");
        this.ok.setIcon(new ImageIcon(this.okIcon));
        this.indietro.addActionListener(b -> {
            this.frame.setVisible(false);
            new SceltaOpzione();
        });
        this.southPanel.add(this.indietro);
        this.southPanel.add(this.ok);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }

    /**
     * 
     * @param args
     */
    public static void main(final String[] args) {
        new Modifica();
    }

}
