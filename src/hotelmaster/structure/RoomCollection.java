package hotelmaster.structure;

import hotelmaster.database.factory.DataFactory;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.utility.collections.TriggeringSet;

/**
 * A collection of rooms, bound to the {@link Hotel}'s {@link DataFactory}.
 */
public interface RoomCollection extends HotelCollection<Room>, TriggeringSet<Room> {

    /**
     * Sets the type of the room to a given value.
     * 
     * @param room
     *            the room to be updated
     * @param type
     *            the type of the room
     * @return true if the room has been updated
     */
    boolean setType(Room room, RoomTypePriceDescriber type);

    /**
     * Adds an extra to the room.
     * 
     * @param room
     *            the room
     * @param extra
     *            the extra to be added
     * @return true if the room has been updated
     */
    boolean addExtra(Room room, RoomExtraPriceDescriber extra);

    /**
     * Removes an extra from the room.
     * 
     * @param room
     *            the room
     * @param extra
     *            the extra to be removed
     * @return true if the room has been updated
     */
    boolean removeExtra(Room room, RoomExtraPriceDescriber extra);

    /**
     * Removes all extras from a room.
     * 
     * @param room
     *            the room
     */
    void clearExtras(Room room);

    @Override
    default void clear() {
        // TODO Auto-generated method stub
        HotelCollection.super.clear();
    }

    /**
     * Instances a new collection of stays.
     * 
     * @return the new instance
     */
    static RoomCollection create() {
        return new RoomCollectionImpl();
    }
}
