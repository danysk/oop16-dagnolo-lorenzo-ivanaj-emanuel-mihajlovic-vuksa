package hotelmaster.structure;

import hotelmaster.database.factory.DataFactory;
import hotelmaster.pricing.PriceCollection;
import hotelmaster.reservations.StayCollection;

/**
 * A modifiable Hotel. All fields are modifiable through getters and modifiable
 * sets.
 */
public interface ModifiableHotel extends Hotel {

    /**
     * Adds a certain amount of floors to the hotel.
     * 
     * @param amount
     *            the amount of floors to add
     * @return the amount of floors in the hotel
     */
    int addFloors(int amount);

    /**
     * Removes the given floor from the hotel.
     * 
     * @param floorToRemove
     *            the floor to remove
     * @return the amount of floors in the hotel
     */
    int removeFloor(int floorToRemove);

    /**
     * Removes a certain amount of floors from the hotel.
     * 
     * @param amount
     *            the amount of floors to remove
     * @return the amount of floors in the hotel
     */
    int removeFloors(int amount);

    /**
     * Returns the number of the next room that will be created on a given
     * floor.
     * 
     * @param floor
     *            the floor
     * @return the number of the room
     * @throws IllegalArgumentException
     *             the floor does not exist
     */
    int nextRoomOnFloor(int floor) throws IllegalArgumentException;

    /**
     * Returns the {@link PriceCollection} of this hotel, containing all of this
     * hotel's prices.
     * 
     * @return the {@link PriceCollection}
     */
    PriceCollection getPrices();

    /**
     * Returns a modifiable list of all the rooms in the hotel. The list does
     * not allow duplicate or null elements.
     * 
     * @return the set of rooms
     */
    RoomCollection getRooms();

    /**
     * Returns a modifiable set of all the stays in the hotel. This is a
     * synchronized set, therefore iteration over this set should be in a
     * synchronized block or method.
     * 
     * @return the set of stays
     */
    StayCollection getStays();

    /**
     * Returns the {@link DataFactory} used by the hotel.
     * 
     * @return the DataFactory
     */
    DataFactory getData();

    /**
     * Returns the singleton instance of ModifiableHotel. The instances of Hotel
     * and ModifiableHotel are the same.
     * 
     * @return the ModifiableHotel singleton
     */
    static ModifiableHotel instance() {
        return HotelImpl.instance();
    }
}
