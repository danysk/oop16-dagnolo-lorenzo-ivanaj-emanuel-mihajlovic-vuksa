package hotelmaster.database.factory;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hotelmaster.database.admin.PriceUtilities;
import hotelmaster.database.admin.Rooms;
import hotelmaster.database.secretary.ModifiableReservation;
import hotelmaster.database.secretary.Reservation;
import hotelmaster.database.utilities.BasicReservationUtilities;
import hotelmaster.database.utilities.DocumentUtilities;
import hotelmaster.database.utilities.PersonPriceUtilities;
import hotelmaster.database.utilities.RoomExtraUtilities;
import hotelmaster.database.utilities.RoomTypeUtilities;
import hotelmaster.database.utilities.RoomUtilities;
import hotelmaster.database.utilities.SeasonUtilities;
import hotelmaster.database.utilities.StayExtraUtilities;
import hotelmaster.database.utilities.StayTypeUtilities;
import hotelmaster.exceptions.GuestException;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.exceptions.UnmodifiablePriceDescriberException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.PriceDescriber;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.pricing.SeasonPriceDescriber;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.reservations.Client;
import hotelmaster.reservations.DocumentType;
import hotelmaster.reservations.ModifiableOccupation;
import hotelmaster.reservations.ModifiableStay;
import hotelmaster.reservations.Stay;
import hotelmaster.structure.ModifiableRoom;
import hotelmaster.structure.Room;
import hotelmaster.utility.time.FixedPeriod;

/**
 * Empty data factory used for testing. It doesn't have any connection with the data source.
 *
 */
public class EmptyDataFactory implements DataFactory {

    @Override
    public PriceUtilities getPrices() {
        return new PriceUtilities() {

            @Override
            public void update(final PriceDescriber price) throws UnmodifiablePriceDescriberException {
            }

            @Override
            public void delete(final PriceDescriber price) throws PriceDescriberRemovalException {
            }

            @Override
            public void create(final PriceDescriber price) {
            }
        };
    }

    @Override
    public Rooms getRooms() {
        return new Rooms() {

            @Override
            public void removeRoom(final Room r) throws RoomRemovalException {
            }

            @Override
            public void removeExtra(final Room r, final RoomExtraPriceDescriber extra) throws RoomRemovalException {
            }

            @Override
            public void modifyTypeOfRoom(final Room r) {
            }

            @Override
            public void createRoom(final Room r) throws RoomRemovalException, IllegalArgumentException {
            }

            @Override
            public void addExtra(final Room r, final RoomExtraPriceDescriber extra) throws IllegalArgumentException {
            }
        };
    }

    @Override
    public BasicReservationUtilities getReservations() {
        return new BasicReservationUtilities() {

            @Override
            public Set<ModifiableStay> loadStays() {
                return new HashSet<>();
            }

            @Override
            public StayTypePriceDescriber getType(final Client c) {
                throw new UnsupportedOperationException("The client is not registered");
            }

            @Override
            public Set<ModifiableRoom> getRooms(final Client c) {
                return new HashSet<>();
            }

            @Override
            public Set<RoomExtraPriceDescriber> getRoomExtras(final Room r) {
                return Collections.emptySet();
            }

            @Override
            public Map<PersonPriceDescriber, Integer> getPeople(final Client c, final Room r) {
                return new HashMap<>();
            }

            @Override
            public Set<StayExtraPriceDescriber> getExtras(final Client c) {
                return new HashSet<>();
            }

            @Override
            public FixedPeriod getDates(final Client c, final ModifiableStay stay) {
                throw new UnsupportedOperationException("The client is not registered");
            }

            @Override
            public Set<Client> getClients() {
                return new HashSet<>();
            }
        };
    }

    @Override
    public RoomExtraUtilities getRoomExtraUtilities() {
        class EmptyRoomExtraUtiities extends RoomExtraUtilities {
            @Override
            public Set<RoomExtraPriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyRoomExtraUtiities();
    }

    @Override
    public RoomTypeUtilities getRoomTypeUtilities() {
        class EmptyRoomTypeUtilities extends RoomTypeUtilities {
            @Override
            public Set<RoomTypePriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyRoomTypeUtilities();
    }

    @Override
    public StayTypeUtilities getStayTypeUtilities() {
        class EmptyStayTypeUtilities extends StayTypeUtilities {
            @Override
            public Set<StayTypePriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyStayTypeUtilities();
    }

    @Override
    public StayExtraUtilities getStayExtraUtilities() {
        class EmptyStayExtraUtilities extends StayExtraUtilities {
            @Override
            public Set<StayExtraPriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyStayExtraUtilities();
    }

    @Override
    public SeasonUtilities getSeasonUtilities() {
        class EmptySeasonUtilities extends SeasonUtilities {
            @Override
            public Set<SeasonPriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptySeasonUtilities();
    }

    @Override
    public DocumentUtilities getDocumentUtilities() {
        class EmptyDocumentUtilities extends DocumentUtilities {
            @Override
            public Set<DocumentType> getAll() {
                Set<DocumentType> documents = new HashSet<>();
                documents.add(new DocumentType("Passaporto", 10));
                return documents;
            }
        }
        return new EmptyDocumentUtilities();
    }

    @Override
    public PersonPriceUtilities getPersonPriceUtilities() {
        class EmptyPersonPriceUtilities extends PersonPriceUtilities {
            @Override
            public Set<PersonPriceDescriber> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyPersonPriceUtilities();
    }

    @Override
    public RoomUtilities getRoomUtilities() {
        class EmptyRoomUtilities extends RoomUtilities {
            @Override
            public Set<ModifiableRoom> getAll() {
                return new HashSet<>();
            }
        }
        return new EmptyRoomUtilities();
    }

    @Override
    public ModifiableReservation getModifiableReservation() {
        return new ModifiableReservation() {

            @Override
            public void removeRoomOccupation(final Stay stay, final ModifiableOccupation occ) {
            }

            @Override
            public void removeExtra(final Stay stay, final StayExtraPriceDescriber extra) {
            }

            @Override
            public void modifyStayType(final Stay stay) {
            }

            @Override
            public void modifyPeople(final Stay stay, final ModifiableOccupation newOccupation) {
            }

            @Override
            public void modifyDates(final Stay stay) {
            }

            @Override
            public void deleteReservation(final Stay stay) {
            }

            @Override
            public void addRoomOccupation(final Stay stay, final ModifiableOccupation occ) {
            }

            @Override
            public void addExtra(final Stay stay, final StayExtraPriceDescriber extra) {
            }
        };
    }

    @Override
    public Reservation getReservation() {
        return new Reservation() {

            @Override
            public void registerStay(final Stay stay) throws GuestException {
            }

            @Override
            public void confirmStay(final Stay stay) {
            }

            @Override
            public void closeStay(final Stay stay) {
            }
        };
    }

}
