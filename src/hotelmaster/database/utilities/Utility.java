package hotelmaster.database.utilities;

import java.util.Set;

/**
 * A generic class for utilities.
 * 
 * @param <T>
 */
public abstract class Utility<T> {

    /**
     * 
     */
    public Utility() { }

    /**
     * Get all the available instances of T mapped with the data source.
     * 
     * @return a set of {@link T}
     */
    public abstract Set<T> getAll();

}
