package hotelmaster.database.utilities;

import java.sql.ResultSet;

import com.google.common.base.Optional;

import hotelmaster.utility.time.FixedPeriod;
/**
 * Hotel statistic.
 *
 */
public interface StatisticUtilities {
    /**
     * Get the total revenue in a range of dates.
     * @param period the period
     * @return the total revenue of this period
     */
    double getTotalEarnings(FixedPeriod period);
    /**
     * Get all the old stays of the hotel.
     * @return a {@link ResultSet} that will be easily inserted in a table.
     */
    Optional<ResultSet> getOldStays();

}