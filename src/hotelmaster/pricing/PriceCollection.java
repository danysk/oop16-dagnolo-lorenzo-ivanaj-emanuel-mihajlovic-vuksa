package hotelmaster.pricing;

import hotelmaster.database.factory.DataFactory;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelCollection;
import hotelmaster.utility.collections.TypeSetMap;

/**
 * A collection of prices, bound to the {@link Hotel}'s {@link DataFactory}.
 */
public interface PriceCollection extends HotelCollection<PriceDescriber>, TypeSetMap<PriceDescriber> {

    /**
     * Sets a price to a new given value.
     * 
     * @param price
     *            the price to be set
     * @param newValue
     *            the new value
     * @return true if the collection has been modified
     */
    boolean setPrice(PriceDescriber price, double newValue);

    @Override
    default void clear() {
        HotelCollection.super.clear();
    }

    /**
     * Instances a new collection of prices.
     * 
     * @return the new instance
     */
    static PriceCollection create() {
        return new PriceCollectionImpl();
    }
}
