package hotelmaster.exceptions;

/**
 * Remove a person price associated with a stay.
 */
public class PersonPriceRemovalException extends HotelMasterException {
    /**
     * 
     */
    public PersonPriceRemovalException() {
        super();
    }

    /**
     * 
     * @param arg0
     *            description
     * @param arg1
     *            cause
     */
    public PersonPriceRemovalException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     * 
     * @param arg0
     *            description
     */
    public PersonPriceRemovalException(final String arg0) {
        super(arg0);
    }

    /**
     * 
     * @param arg0
     *            cause
     */
    public PersonPriceRemovalException(final Throwable arg0) {
        super(arg0);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
