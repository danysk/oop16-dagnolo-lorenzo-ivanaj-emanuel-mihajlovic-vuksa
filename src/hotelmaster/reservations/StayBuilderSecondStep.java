package hotelmaster.reservations;

import java.util.Map;

import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.exceptions.OccupiedRoomException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.structure.Room;

/**
 * Allows setting the rooms.
 */
public interface StayBuilderSecondStep extends StayBuilder {
    /**
     * Adds a room to this stay. Must set the start and end dates and the
     * parameters related to the client before calling. Once this function is
     * successfully called, the parameters that have been previously set are
     * locked. Only "complete" and this function can be called from then on.
     * 
     * @param room
     *            the room to be added
     * @param people
     *            the people in the room
     * @return this StayBuilder instance
     * @throws MissingEntityException
     *             a given PersonPriceDescriber does not exist in the hotel
     * @throws IllegalArgumentException
     *             the room cannot hold the given amount of people
     * @throws IllegalStateException
     *             the dates or client-related parameters have not been all set
     * @throws OccupiedRoomException
     *             the room is occupied during the set dates
     */
    StayBuilderSecondStep addRoom(Room room, Map<PersonPriceDescriber, Integer> people)
            throws MissingEntityException, IllegalArgumentException, IllegalStateException, OccupiedRoomException;

    /**
     * Completes the StayBuilder, disabling further function calls and writing
     * the stay to the hotel.
     * 
     * @throws IllegalStateException
     *             the functions haven't been properly called
     */
    void complete() throws IllegalStateException;
}
